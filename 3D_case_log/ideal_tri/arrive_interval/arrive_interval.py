#!/usr/bin/python

#this script is tried to down the sequence arrive speed of the contiguous request.

infileNames = ["../gpu_request.log", "../cmd.trace", "../idx.trace", "../vtx.trace", "../tex.trace", "../tilec.trace", "../tilez.trace", "../frame.trace", "../depth.trace", "../resolve.trace"]
granu =  100000 #the unit is ns

for infileName in infileNames:
    indexPoint = infileName.rfind(".")
    outfileName = infileName[3:indexPoint] + "_arrive_interval.log"

    infile = open(infileName)
    intervalData = {}
    preArriveTime = 0
    for line in infile.readlines():
	strArray = line.split()
	arriveTime = int(strArray[0])
	if preArriveTime == 0:
	    preArriveTime = arriveTime
	    continue
	interval = arriveTime - preArriveTime
	interval /= granu
	if intervalData.has_key(interval):
	    intervalData[interval] += 1
	else:
	    intervalData[interval] = 1

    infile.close()
    
    outfile = open(outfileName,'w')
    intervals = intervalData.keys()
    intervals.sort()
    for interval in intervals:
	outfile.write("%u, %u, \n" % (interval, intervalData[interval]))
    outfile.close()

    
