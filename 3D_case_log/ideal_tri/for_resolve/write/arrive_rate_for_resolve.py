#!/usr/bin/python
#this arrive_rate script is specially for resovle file

granu = 20000

def calculateArriveRate(infileName):
    infile = open(infileName + ".trace")
    outfile = open(infileName + "_arrive_rate.log", 'w')
    
    arriveTimePre = 0
    countGranu = 0
    for line in infile.readlines():
	strArray = line.split()
	arriveTimeCur = int(strArray[0])
	if arriveTimePre == 0:
	    arriveTimePre = arriveTimeCur
	    countGranu = 1
	elif arriveTimeCur < arriveTimePre + granu:
	    countGranu += 1
	else:
	    outfile.write("%u\n" % countGranu)
	    arriveTimePre += granu
	    while arriveTimePre + granu <= arriveTimeCur:
		outfile.write("0\n")
		arriveTimePre += granu
	    countGranu = 1
	
    infile.close()
    outfile.close()

	    
	

#########################################################
num_level1 = 12 #this value can be modified
num_level2 = 4 #this value can be modified too

index_level1 = 0
while index_level1 < num_level1:
    calculateArriveRate("%u" % index_level1)
    index_level2 = 0
    while index_level2 < num_level2:
	calculateArriveRate("%u_%u" % (index_level1, index_level2))
	index_level2 += 1
    index_level1 += 1

