#!/usr/bin/python
#this file has two aims:
#1\split the file into split files: this file consits of 1024 lines and each file was split into smaller files, each of which consists of 64x4 lines
#2\calculate the interval of lines in each of above file

infileName = "../../resolve_write.trace"
infile = open(infileName)

numOfLine = 0

NUM_LEVEL1 = 1024

index_level1 = 0
#index_level2 = 0 have not used


######split the file#################
for line in infile.readlines():
    
    strArray = line.split()
    addressCur = int(strArray[5], 16)
    if numOfLine % NUM_LEVEL1 == 0:
	outfileTrace = open("%d.trace" % index_level1, 'w')
	outfileTraceLevel2_0 = open("%d_0.trace" % index_level1, 'w')
	outfileTraceLevel2_1 = open("%d_1.trace" % index_level1, 'w')
	outfileTraceLevel2_2 = open("%d_2.trace" % index_level1, 'w')
	outfileTraceLevel2_3 = open("%d_3.trace" % index_level1, 'w')

	index_level1 += 1 #for next use

#	baseLevel2_0 = addressCur                  #the next 4 line represents the for thread
#	baseLevel2_1 = addressCur + 0x80
#	baseLevel2_2 = addressCur + 0x100
#	baseLevel2_3 = addressCur + 0x180
	
#	addressPreLevel2_0 = baseLevel2_0 - 0x40
#	addressPreLevel2_1 = baseLevel2_1 - 0x40
#	addressPreLevel2_2 = baseLevel2_2 - 0x40
#	addressPreLevel2_3 = baseLevel2_3 - 0x40

    outfileTrace.write(line)
    
    # if addressCur == addressPreLevel2_0 + 0x200 or addressCur == addressPreLevel2_0 + 0x40 or addressCur == baseLevel2_0:
    if addressCur & 0x1ff == 0x00 or addressCur & 0x1ff == 0x40 :
	outfileTraceLevel2_0.write(line)
	addressPreLevel2_0 = addressCur
    # elif addressCur == addressPreLevel2_1 + 0x200 or addressCur == addressPreLevel2_1 + 0x40 or addressCur == baseLevel2_1:
    elif addressCur & 0x1ff == 0x080 or addressCur & 0x1ff == 0x0c0:
	outfileTraceLevel2_1.write(line)
	addressPreLevel2_1 = addressCur
    # elif addressCur == addressPreLevel2_2 + 0x200 or addressCur == addressPreLevel2_2 + 0x40 or addressCur == baseLevel2_2:
    elif addressCur & 0x1ff == 0x100 or addressCur & 0x1ff == 0x140:
	outfileTraceLevel2_2.write(line)
	addressPreLevel2_2 = addressCur
    # elif addressCur == addressPreLevel2_3 + 0x200 or addressCur == addressPreLevel2_3 + 0x40 or addressCur == baseLevel2_3:
    elif addressCur & 0x1ff == 0x180 or addressCur & 0x1ff == 0x1c0:
	outfileTraceLevel2_3.write(line)
	addressPreLevel2_3 = addressCur

    numOfLine += 1
    if(numOfLine % 1024 == 0):
	outfileTrace.close()
	outfileTraceLevel2_0.close()
	outfileTraceLevel2_1.close()
	outfileTraceLevel2_2.close()
	outfileTraceLevel2_3.close()
    
#########calculate the interval of the request##############
num_level1 = index_level1
index_level1 = 0
index_level2 = 0

def calculateInterval(fileName):
    infile = open(fileName + ".trace")
    outfile = open(fileName + "_interval.log", 'w')
    outfileCum = open(fileName + "_interval_cum.log", 'w')
    arriveTimePre = 0
    intervalCum = 0
    for line in infile.readlines():
	strArray = line.split()
	arriveTimeCur = int(strArray[0])
	if arriveTimePre != 0:
	    outfile.write("%d\n" % (arriveTimeCur - arriveTimePre))
	    intervalCum = intervalCum + arriveTimeCur - arriveTimePre
	    outfileCum.write("%d\n" % intervalCum)
	arriveTimePre = arriveTimeCur
    outfile.close()
    outfileCum.close()
    infile.close()

def calculateInterval_2(fileName): #make the even of every two contiguous intervals.
    infile = open(fileName + ".trace")
    outfile = open(fileName + "_interval_2.log", 'w')
    outfileCum = open(fileName + "_interval_2_cum.log", 'w')
    arriveTimePre = 0
    interval_2 = 0
    intervalCum_2 = 0
    num = 0
    for line in infile.readlines():
	strArray = line.split()
	arriveTimeCur = int(strArray[0])
	if arriveTimePre != 0:
	    interval_2 = interval_2 + arriveTimeCur - arriveTimePre
	    intervalCum_2 = intervalCum_2 +arriveTimeCur - arriveTimePre
	    num += 1
	arriveTimePre = arriveTimeCur
	num = num & 0x1
	if num == 0 and arriveTimePre != 0:
	    outfile.write("%u\n" % interval_2)
	    outfileCum.write("%u\n" % intervalCum_2)
	    interval_2 = 0
    infile.close()
    outfile.close()
    outfileCum.close()

while index_level1 < num_level1:
    calculateInterval("%u" % index_level1)
    index_level2 = 0
    while index_level2 < 4:
	calculateInterval("%u_%u" % (index_level1, index_level2))
	calculateInterval_2("%u_%u" % (index_level1, index_level2))
	index_level2 += 1
    index_level1 += 1
	
    
