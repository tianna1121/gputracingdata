#!/usr/bin/python

#this file is tried to find the address increasing rule of the vtx.trace
#from the head section of this file, the rugular seems to be that:
#1\there are two total rules
#2\each rule increase monotonously by 0x40
#so this script aims to see whether the number of rules are really 2; and if not, find the start of the other rules

infileName = "vtx.trace"
infile = open(infileName)

addressPre1 = 0
addressPre2 = 0
num = 0
for line in infile.readlines():
    strArray = line.split()
    addressCur = int(strArray[5], 16)
    if addressPre1 == 0:
	addressPre1 = addressCur
    elif addressCur == addressPre1 + 0x40:
	addressPre1 += 0x40
    elif addressPre2 == 0:
	addressPre2 = addressCur
    elif addressCur == addressPre2 + 0x40:
	addressPre2 += 0x40
    else:
	print strArray[5]
	
infile.close()
