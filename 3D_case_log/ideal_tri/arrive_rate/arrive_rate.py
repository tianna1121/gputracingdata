#!/usr/bin/python

#this script is tried to down the sequence arrive speed of the contiguous request.

infileNames = ["../gpu_request.log", "../cmd.trace", "../idx.trace", "../vtx.trace", "../tex.trace", "../tilec.trace", "../tilez.trace", "../frame.trace", "../depth.trace", "../resolve.trace"]

granu =  6000 #the unit is ns

for infileName in infileNames:
    indexPoint = infileName.rfind(".")
    outfileName = infileName[3:indexPoint] + "_arrive_rate.log"

    infile = open(infileName)
    outfile = open(outfileName,'w')
    preArriveTime = 0
    countGranu = 0
    for line in infile.readlines():
	strArray = line.split()
	arriveTime = int(strArray[0])
	if preArriveTime == 0:
	    preArriveTime = arriveTime
	    countGranu = 0
	if arriveTime <= preArriveTime + granu:
	    countGranu += 1
	else:
	    outfile.write("%u, " % countGranu)
	    countGranu = 1
	    preArriveTime += granu
	    while(preArriveTime + granu < arriveTime):
		outfile.write("0, ")
		preArriveTime += granu
    infile.close()
    outfile.close()
