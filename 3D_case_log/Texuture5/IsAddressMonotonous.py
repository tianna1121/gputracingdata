#!/usr/bin/python

#this script aims to see if the address of this the trace is monotonous

infileName = "idx.trace"
infile = open(infileName)

numOfLine = 0
addressPre = 0
for line in infile.readlines():
    numOfLine += 1
    strArray = line.split()
    addressCur = int(strArray[5], 16)
    if addressCur < addressPre:
	print "%8u :  %08x" % (numOfLine-1, addressPre)
	print "%8u :  %08x" % (numOfLine, addressCur)
	print
    addressPre = addressCur
