#!/usr/bin/python 

infileName = 'gpu_request.log'

CMD_BUFFER_START = 0x130000
IDX_BUFFER_START = 0x130540
VTX_BUFFER_START = 0x1a5880
TEX_BUFFER_START = 0x21be80
TILEC_BUFFER_START = 0x271480
TILEZ_BUFFER_START = 0x2764c0

FRAME_BUFFER_START = 0x1930000
DEPTH_BUFFER_START = 0x1d30000
RESOLVE_BUFFER_START = 0x3530000

outfileCmd = open("cmd.trace", 'w')
outfileIdx = open("idx.trace", 'w')
outfileVtx = open("vtx.trace", 'w')
outfileTex = open("tex.trace", 'w')
outfileTilec = open("tilec.trace", 'w')
outfileTilez = open("tilez.trace", 'w')
outfileFrame = open("frame.trace", 'w')
outfileDepth = open("depth.trace", 'w')
outfileResolve = open("resolve.trace", 'w')

infile = open(infileName)
for line in infile.readlines():
    strArray = line.split()
    address = int(strArray[5], 16)
    if address >= CMD_BUFFER_START and address < IDX_BUFFER_START:
	outfileCmd.write(line )
    elif address >= IDX_BUFFER_START and address < VTX_BUFFER_START:
	outfileIdx.write(line)
    elif address >= VTX_BUFFER_START and address < TEX_BUFFER_START:
	outfileVtx.write(line)
    elif address >= TEX_BUFFER_START and address < TILEC_BUFFER_START:
	outfileTex.write(line)
    elif address >= TILEC_BUFFER_START and address < TILEZ_BUFFER_START:
	outfileTilec.write(line)
    elif address >= TILEZ_BUFFER_START and address < FRAME_BUFFER_START:
	outfileTilez.write(line)
    elif address >= FRAME_BUFFER_START and address < DEPTH_BUFFER_START:
	outfileFrame.write(line)
    elif address >= DEPTH_BUFFER_START and address < RESOLVE_BUFFER_START:
	outfileDepth.write(line)
    else: #the rest is the references to resolve buffer area
	outfileResolve.write(line)

outfileCmd.close()
outfileIdx.close()
outfileVtx.close()
outfileTex.close()
outfileTilec.close()
outfileTilez.close()
outfileFrame.close()
outfileDepth.close()
outfileResolve.close()
