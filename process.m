close all
clear
clc

inputParams = {'Tex', 'VTX'};
h = figure();
suptitle('Address vs Time');

for iTraces = 1:length(inputParams)  
    fid = eval(['fopen(''awk',char(inputParams(iTraces)),'Trace.txt'');']);
    a = fscanf(fid,'%d  %x  %x');
    fclose(fid);
    
    duTicks = [];
    startAddr = [];
    endAddr = [];
    
    for i = 0:length(a)/3-1
        duTicks(i+1,1) = a(i*3+1);
    end
    
    for i = 0:length(a)/3-2
        startAddr(i+1,1) = a(i*3+2);
    end
    startAddr(end+1,1) = a(end-1);
    
    for i = 0:length(a)/3-3
        endAddr(i+1,1) = a(i*3+3);
    end
    endAddr(end+1,1) = a(end-3);
    endAddr(end+1,1) = a(end);

    %eval(['duTicks',char(inputParams(iTraces)),' = duTicks']);
    %eval(['startAddr',char(inputParams(iTraces)),' = startAddr']);
    %eval(['endAddr',char(inputParams(iTraces)),' = endAddr']);
    
    subplot(2,2,iTraces);
    %title(char(inputParams(iTraces)));
    plot(duTicks, startAddr, 'b.');
    hold on;
    plot(duTicks, endAddr, 'r.');
end
